# Telegraudio
Self hosted Telegram bot to download audio from various sites

Setup

1. Make a new bot with [@botfather](https://www.t.me/botfather) and grab the API Key
2. Rename .env-example to .env and put the API key in the `API_Token` variable
3. Install requirements via `pip install -r requirements.txt`in your virtual environment or main python installtion
4. Run `bot.py`
